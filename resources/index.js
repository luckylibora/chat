/**
 * Created by lucky on 05.11.16.
 */

require('./logger');

const intel = require('intel');
const mongo = require('../mongo/mongo');
const redis = require('../redis');
const services = require('../services');



/**
 *
 * dispose all used resources
 *
 * @returns {Promise}
 */
function dispose() {
    return Promise.all([mongo.dispose(), redis.dispose()])
        .then(() => intel.info('All resources disposed'))
        .catch(err => intel.error(err));
}


/**
 *
 * initialize all resources
 *
 * @returns {Promise}
 */
function init() {
    return mongo.init()
        .then(() => services.init())
        .then(() => intel.info('All resources inited'))
        .catch(err => {
            intel.error('Failed to load resources', err);
            throw err;
        });
}


exports.dispose = dispose;

exports.init = init;
