/**
 * Created by lucky on 05.11.16.
 */

const intel = require('intel');


intel.basicConfig({
    format: '[%(date)s] %(levelname)s: %(message)s'
});


module.exports = intel;
