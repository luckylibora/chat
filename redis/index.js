/**
 * Created by lucky on 05.11.16.
 */

const intel = require('intel');
const redis = require('redis');
const config = require('../config');


const client = createClient();


/**
 *
 * @param {Object = null} opts
 * @returns {RedisClient}
 */
function createClient(opts) {
    const redisOpts = Object.assign(opts || {}, config.redis);
    const client = redis.createClient(redisOpts);
    client.on('error', err => intel.error(err));
    return client;
}


/**
 *
 * @returns {Promise}
 */
function dispose() {
    client.quit();
    return Promise.resolve();
}


/**
 *
 * @param {string} key
 * @returns {Promise}
 */
function get(key) {
    return new Promise((s, e) => {
        return redis.get(key, (err, res) => err ? e(err) : s(res));
    });
}


/**
 *
 * @param {string} key
 * @param {string} value
 * @returns {Promise}
 */
function set(key,value) {
    return new Promise((s,e) => {
        return redis.set(key, value, err => err ? e(err) : s());
    });
}


exports.client = client;

exports.createClient = createClient;

exports.dispose = dispose;

exports.get = get;

exports.set = set;