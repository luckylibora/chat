/**
 * Created by lucky on 06.11.16.
 */

module.exports = ['$routeProvider', $routeProvider => {
    return $routeProvider.when('/chat/:id?', {
        templateUrl: '/views/chat',
        controller: 'chatController'
    }).otherwise({
        redirectTo: '/chat'
    });
}];
