/**
 * Created by lucky on 06.11.16.
 */

const io = require('../io');


module.exports = ['$scope', '$http', '$routeParams', ($scope, $http, $routeParams) => {

    let messageList = null;


    /**
     *
     * open or create new dialog with user
     * @param {string} user
     */
    $scope.openDialog = user => {
        io.emit('getDialog', {user});
        io.on('getDialog', data => {
            location.hash = '#/chat/' + data._id;
        });
    };


    /**
     *
     * send message from input to server
     */
    $scope.sendMessage = () => {
        if (!$scope.newMessage || !$scope.newMessage.length || io.disconnected) {
            return;
        }
        io.emit('sendMessage', {
            chatId: $scope.activeChat._id,
            message: $scope.newMessage
        });
        $scope.newMessage = '';
    };


    /**
     *
     * get chat by id
     *
     * @param {number} id
     * @returns {Object | null}
     */
    function getChat(id) {
        return $scope.chats.find(c => c._id == id);
    }


    /**
     *
     * load user chats
     * handling newChat event
     *
     */
    function initChats() {
        io.once('chats', data => {
            $scope.chats = data;
            $scope.chats.forEach(c => prepareChat(c));
            let activeChat = null;
            if ($routeParams.id) {
                activeChat = getChat($routeParams.id);
            }
            if (!activeChat) {
                activeChat = $scope.chats.reduce((c1, c2) => c1.lastModified > c2.lastModified ? c1 : c2, $scope.chats[0]);
                return location.hash = '#/chat/' + activeChat._id;
            }
            $scope.activeChat = activeChat;
            $scope.$apply();
            initMessages();
            io.on('newChat', data => {
                $scope.chats.push(prepareChat(data));
                $scope.$apply();
                io.emit('newChat', data);
            });
        });
    }


    /**
     *
     * load active chat messages
     * handling message event
     *
     */
    function initMessages() {
        io.emit('chatMessages', {id: $scope.activeChat._id});
        io.on('chatMessages', data => {
            $scope.activeChat.messages = data.map(msg => prepareMessage(msg));
            $scope.$apply();
            scroll();
        });
        io.on('message', data => {
            const chat = getChat(data.chatId);
            if (!chat) {
                return;
            }
            chat.lastModified = (data.date > chat.lastModified) ? data.date : chat.lastModified;
            chat.messages.push(prepareMessage(data));
            if (chat._id != $scope.activeChat._id) {
                chat.unreadMessages++;
            }
            $scope.$apply();
            scroll();
        });
    }


    /**
     *
     * request to server to sync socket io and express sessions
     * send login event
     *
     * @returns {Promise}
     */
    function initSession() {
        return $http.get('/test')
            .then(() => io.emit('login'));
    }


    /**
     *
     * load all needed data
     * handling reconnect to socket.io
     *
     * @returns {Promise}
     */
    function init() {
        io.on('disconnect', () => {
            io.once('connect', () => init())
        });
        return initSession()
            .then(() => initChats());
    }


    /**
     *
     * add required fields to chat object
     *
     * @param {Object} chat
     * @returns {Object}
     */
    function prepareChat(chat){
        chat.messages = [];
        chat.unreadMessages = 0;
        return chat;
    }


    /**
     *
     * add required fields to message object
     *
     * @param {Object} message
     * @returns {Object}
     */
    function prepareMessage(message) {
        if (!message.date) {
            message.date = Date.now();
        }
        message.dateS = new Date(message.date).toTimeString().split(' ')[0];
        message.id = Math.random();
        return message;
    }


    /**
     *
     * scroll message list to bottom
     *
     */
    function scroll() {
        if (!messageList) {
            messageList = document.getElementById('message-list');
        }
        messageList.scrollTop = messageList.scrollHeight
    }


    init();


}];

