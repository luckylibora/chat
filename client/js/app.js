/**
 * Created by lucky on 05.11.16.
 */

require('./style');
const angular = require('angular');
const angularRoute = require('angular-route');
const controllers = require('./controllers');
const router = require('./router');


const app = angular.module('chat', [angularRoute]);


controllers.forEach(controller => app.controller.apply(null, controller));


app.config(router);

angular.element(document)
    .ready(() => angular.bootstrap(document, ['chat']));
