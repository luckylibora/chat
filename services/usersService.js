/**
 * Created by lucky on 06.11.16.
 */

const chatService = require('./chatsService');
const collection = require('../mongo/usersCollection');
const utils = require('../utils');


/**
 *
 * @param {string} name
 * @param {string} password
 * @returns {Promise}
 */
function check(name, password) {
    if (!name || !name.length || !password || !password.length) {
        return Promise.reject(new Error(`Not valid data ${name} ${password}`))
    }
    const hash = utils.hash(password);
    return collection.getByNamePassword(name, hash);
}


/**
 *
 * @param {string} name
 * @param {string} password
 * @returns {Promise}
 */
function create(name, password) {
    if (!name || !name.length || !password || !password.length) {
        return Promise.reject(new Error(`Not valid data ${name} ${password}`))
    }
    const hash = utils.hash(password);
    return collection.insert({name, password: hash})
        .then(() => chatService.addUserToAll(name));
}


/**
 *
 * @param {string} name
 * @returns {Promise}
 */
function getByName(name) {
    return collection.getByName(name);
}


/**
 *
 * @returns {Promise}
 */
function init() {
    return collection.init();
}


exports.check = check;

exports.create = create;

exports.getByName = getByName;

exports.init = init;