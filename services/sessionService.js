/**
 * Created by lucky on 05.11.16.
 */

const connectRedis = require('connect-redis');
const expressSession = require('express-session');
const config = require('../config');
const redis = require('../redis');


/**
 *
 * @param {Session} session
 * @returns {RedisStore}
 */
function sessionStore(session) {
    return new (connectRedis(session))({client: redis.createClient()});
}


/**
 *
 * @returns {Function}
 */
function session() {
    return expressSession({
        cookie: {
            maxAge: config.session.maxAge,
            domain: config.domain
        },
        name: config.session.name,
        resave: false,
        saveUninitialized: true,
        secret: config.session.secret,
        store: sessionStore(expressSession)
    });
}


exports.session = session;