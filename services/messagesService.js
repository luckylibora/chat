/**
 * Created by lucky on 07.11.16.
 */


const collection = require('../mongo/messagesCollection');


/**
 *
 * @param {string} chatId
 * @returns {Promise}
 */
function getByChatId(chatId) {
    if (!chatId) {
        return Promise.reject(`Not valid arg ${chatId}`);
    }
    return collection.getByChatId(chatId);
}


/**
 *
 * @returns {Promise}
 */
function init() {
    return collection.init();
}


/**
 *
 * @param {string} chatId
 * @param {string} user
 * @param {string} message
 * @param {Date} date
 * @returns {Promise}
 */
function insert(chatId, user, message, date) {
    return collection.insert(chatId, user, message, date);
}


exports.getByChatId = getByChatId;

exports.insert = insert;

exports.init = init;

