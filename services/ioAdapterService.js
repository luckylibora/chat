/**
 * Created by lucky on 05.11.16.
 */

const redis = require('../redis');
const socketIORedis = require('socket.io-redis');


/**
 *
 * @returns {RedisAdapter}
 */
function adapter() {
    return socketIORedis({
        pubClient: redis.createClient({return_buffers: true}),
        subClient: redis.createClient({return_buffers: true})
    });
}


exports.adapter = adapter;