/**
 * Created by lucky on 06.11.16.
 */

const chatsService = require('./chatsService');
const messagesService = require('./messagesService');
const userService = require('./usersService');


/**
 *
 * @returns {Promise}
 */
function init() {
    return Promise.all([
        chatsService.init(),
        messagesService.init(),
        userService.init()
    ]);
}


exports.init = init;
