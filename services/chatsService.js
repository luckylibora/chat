/**
 * Created by lucky on 06.11.16.
 */

const collection = require('../mongo/chatsCollection');
const messageService = require('./messagesService');


/**
 *
 * @param {string} id
 * @param {string} user
 * @returns {Promise}
 */
function addUser(id, user) {
    if (!id || !user) {
        return Promise.reject(new Error(`Invalid args ${id}  ${user}`));
    }
    return collection.addUser(id, user);
}


/**
 *
 * @param {string} user
 * @returns {Promise}
 */
function addUserToAll(user) {
    return collection.getByName('All')
        .then(chat => addUser(chat._id, user));
}


/**
 *
 * @returns {Promise}
 */
function createAllChat(){
    return collection.getByName('All')
        .then(chat => {
            if (!chat) {
                return collection.insert('All', []);
            }
        });
}



/**
 *
 * @param {string} user
 * @returns {Promise}
 */
function getByUser(user) {
    if (!user || !user.length) {
        return Promise.reject(`Not valid arg ${user}`)
    }
    return collection.getByUser(user);
}


/**
 *
 * @returns {Promise}
 * @param {string} user1
 * @param {string} user2
 */
function getOrCreateDialog(user1, user2) {
    if (!user1 || !user2) {
        return Promise.reject(`Not valid args ${user1} ${user2}`);
    }
    return collection.getOrCreateDialog(user1, user2);
}


/**
 *
 * @returns {Promise}
 */
function init() {
    return createAllChat();
}



/**
 *
 * @param {string} id
 * @param {string} user
 * @param {string} message
 * @returns {Promise}
 */
function sendMessage(id, user, message) {
    if (!id || !user || !message) {
        return Promise.reject(new Error(`Not valid args ${id} ${user} ${message}`));
    }
    const now = new Date();
    return Promise.all([
        messageService.insert(id, user, message, now),
        collection.updateLastModified(id, now)
    ]);
}


exports.addUser = addUser;

exports.addUserToAll = addUserToAll;

exports.getByUser = getByUser;

exports.getOrCreateDialog = getOrCreateDialog;

exports.init = init;

exports.sendMessage = sendMessage;