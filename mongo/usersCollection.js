/**
 * Created by lucky on 06.11.16.
 */

'use strict';


const Collection = require('./Collection');


class UsersCollection extends Collection {

    constructor() {
        super('users');
    }


    /**
     *
     * @param {string} name
     * @returns {Promise}
     */
    getByName(name) {
        return this.collection()
            .findOne({name});
    }


    /**
     *
     * @param {string} name
     * @param {string} password
     * @returns {Promise}
     */
    getByNamePassword(name, password) {
        return this.collection()
            .findOne({name, password});
    }


    /**
     *
     * @returns {Promise}
     */
    init() {
        return this.collection()
            .createIndex('name', {unique: true});
    }


    /**
     *
     * @param {Object} user
     * @returns {Promise}
     */
    insert(user) {
        return this.collection()
            .insertOne(user);
    }

}


/**
 *
 * @type {UsersCollection}
 */
module.exports = new UsersCollection();