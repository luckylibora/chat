/**
 * Created by lucky on 06.11.16.
 */

'use strict';


const Collection = require('./Collection');


class ChatsCollection extends Collection {

    constructor() {
        super('chats');
    }


    /**
     *
     * @param {string} id
     * @param {string} user
     * @returns {Promise}
     */
    addUser(id, user) {
        return this.collection()
            .updateOne({_id: this.objectId(id)}, {$addToSet: {users: user}});
    }


    /**
     *
     * @param {string} userName
     * @returns {Promise}
     */
    getByUser(userName) {
        return this.collection()
            .find({users: {$elemMatch: {$eq: userName}}})
            .limit(20)
            .toArray();
    }


    getByName(name) {
        return this.collection()
            .findOne({name});

    }


    /**
     *
     * @returns {Promise}
     * @param {string} user1
     * @param {string} user2
     */
    getOrCreateDialog(user1, user2) {
        const users = [user1, user2].sort();
        const name = users.join(',');
        return this.collection()
            .findOne({users: {$size: 2, $in: users}, name})
            .then(chat => {
                if (chat) {
                    return chat;
                }
                return this.insert(name, users)
                    .then(() => this.getOrCreateDialog(user1, user2));
            });

    }


    /**
     *
     * @param {string} name
     * @param {Array.<string>} users
     * @returns {Promise}
     */
    insert(name, users) {
        return this.collection()
            .insertOne({users, name, lastModified: new Date()})
    }


    /**
     *
     * @param {string} id
     * @param {Date} lastModified
     * @returns {Promise}
     */
    updateLastModified(id, lastModified) {
        return this.collection()
            .updateOne({_id: this.objectId(id)}, {$set: {lastModified}});
    }
}


/**
 *
 * @type {ChatsCollection}
 */
module.exports = new ChatsCollection();