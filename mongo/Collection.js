/**
 * Created by lucky on 06.11.16.
 */

'use strict';


const mongo = require('./mongo');


/**
 *
 * Base class for mongo collections
 *
 */
class Collection {

    constructor(name){
        this.name = name;
        this._collection = null;
    }


    /**
     *
     * @returns {Collection|null}
     */
    collection() {
        if (!this._collection) {
            this._collection = mongo.db().collection(this.name);
        }
        return this._collection;
    }


    /**
     *
     * @returns {Promise}
     */
    init() {
        return Promise.resolve();
    }


    /**
     *
     * @param {string} id
     * @returns {*}
     */
    objectId(id) {
        return mongo.objectId(id);
    }

}


module.exports = Collection;