/**
 * Created by lucky on 06.11.16.
 */

'use strict';

const intel = require('intel');
const mongodb = require('mongodb');
const config = require('../config');


const MongoClient = mongodb.MongoClient;


/**
 *
 * @type {Db | null}
 */
let _db = null;


/**
 *
 * @returns {Promise}
 */
function connect() {
    return MongoClient.connect(config.mongo)
        .then(res => {
            _db = res;
            intel.info('MongoDB inited');
        })
        .catch(err => {
            intel.error(err);
            throw err;
        });
}


/**
 *
 * @returns {Db|null}
 */
function db() {
    return _db;
}


/**
 *
 * @returns {Promise}
 */
function dispose() {
    return _db ? _db.close() : Promise.resolve();
}


/**
 *
 * @returns {Promise}
 */
function init() {
    return connect();
}


/**
 *
 * @param {string} id
 * @returns {*}
 */
function objectId(id){
    return mongodb.ObjectID(id);
}


exports.db = db;

exports.dispose = dispose;

exports.init = init;


exports.objectId = objectId;