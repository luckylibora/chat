/**
 * Created by lucky on 07.11.16.
 */

'use strict';


const Collection = require('./Collection');


class MessagesCollection extends Collection {

    constructor() {
        super('messages');
    }


    /**
     *
     * @param {string} chatId
     * @returns {Promise}
     */
    getByChatId(chatId) {
        return this.collection()
            .find({chatId})
            .sort({date: -1})
            .limit(20)
            .toArray();
    }

    /**
     *
     * @returns {Promise}
     */
    init() {
        return this.collection()
            .createIndex({chatId: 1});
    }


    /**
     *
     * @param {string} chatId
     * @param {string} user
     * @param {string} message
     * @param {Date} date
     * @returns {Promise}
     */
    insert(chatId, user, message, date) {
        return this.collection()
            .insertOne({chatId, user, message, date});
    }

}



module.exports = new MessagesCollection();