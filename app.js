const bodyParser = require('body-parser');
const compression = require('compression');
const cookieParser = require('cookie-parser');
const express = require('express');
const expressSocketIOSession = require('express-socket.io-session');
const favicon = require('serve-favicon');
const http = require('http');
const logger = require('morgan');
const path = require('path');
const socketio = require('socket.io');
const ioAdapterService = require('./services/ioAdapterService');
const routes = require('./routes');
const resources = require('./resources');
const sessionsService = require('./services/sessionService');
const socketRouter = require('./routes/socket');


const app = express();
const server = http.createServer(app);
const io = socketio(server, {transports: ['websocket', 'polling']});


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());


const session = sessionsService.session();

app.use(session);

io.use(expressSocketIOSession(session));

io.adapter(ioAdapterService.adapter());

app.use(compression({level: 1}));
app.use('/public', express.static(path.join(__dirname, 'public')));

app.use('/', (req, res, next) => {
    res.io = io;
    return next();
});

app.use('/', routes);

socketRouter(io);


exports.app = app;

exports.server = server;


