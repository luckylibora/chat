/**
 * Created by lucky on 05.11.16.
 */

const intel = require('intel');


/**
 *
 * Get variable from environment by name or by prefix for objects
 *
 * @param {string} key
 * @returns {*}
 */
function getFromEnv(key) {
    const temp = process.env[key];
    if (temp) {
        return temp;
    }
    const res = {};
    Object.keys(process.env)
        .filter(k => k.indexOf('_') != -1)
        .forEach(k => {
            const temp = k.split('_');
            if (temp[0] == key) {
                res[temp[1]] = process.env[k];
            }
        });
    return res;
}


/**
 *
 */
function init() {
    const session = getFromEnv('session') || {};
    session.maxAge = session.maxAge || 259200000;
    session.name = session.name || 'chat';
    session.secret = session.secret || 'chat_secret';
    exports.session = session;
    exports.redis = getFromEnv('redis');
    exports.mongo = getFromEnv('mongo') || 'mongodb://localhost:27017/chat';
}


init();