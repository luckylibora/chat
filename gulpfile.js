/**
 * Created by lucky on 05.11.16.
 */

const autoprefixer = require('autoprefixer');
const del = require('del');
const babel = require('gulp-babel');
const babelify = require('babelify');
const browserify = require('gulp-browserify');
const concatCss = require('gulp-concat-css');
const gulp = require('gulp');
const minifyCss = require('gulp-minify-css');
const plumber = require('gulp-plumber');
const postcss = require('gulp-postcss');
const uglify = require('gulp-uglify');


const jsDest = 'public/js';
const jsSrc = 'client/js/**/*.js';


//dev


//scripts


gulp.task('clean-js', cb => {
    del(jsDest + '/*.*').then(() => cb())
});


function js() {
    return gulp.src(['client/js/app.js', 'client/js/login.js'])
        .pipe(plumber())
        .pipe(browserify({
            transform: babelify.configure({
                presets: ['es2015']
            }),
            insertGlobals: true
        }));
}


gulp.task('browserify', ['clean-js'],
    () => js()
        .pipe(gulp.dest(jsDest))
);


gulp.task('scripts', ['browserify']);


//styles

const cssDest = 'public/css';
const cssSrc = 'client/css/*.css';


gulp.task('clean-css', cb => {
    del(cssDest + '/*.*').then(() => cb())
});


function css() {
    return gulp.src(cssSrc)
        .pipe(concatCss('index.css'));
}


gulp.task('concat-css', ['clean-css'],
    () => css()
        .pipe(gulp.dest(cssDest))
);


gulp.task('default', ['scripts', 'concat-css'], () => {
    gulp.watch(jsSrc, ['scripts']);
    gulp.watch(cssSrc, ['concat-css']);
});


//production

//scripts


gulp.task('uglify', ['browserify'],
    () => js()
        .pipe(uglify())
        .pipe(gulp.dest(jsDest))
);

gulp.task('scripts-prod', ['uglify']);


//styles

gulp.task('css-minify', ['clean-css'],
    () => css()
        .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe(postcss([autoprefixer({browsers: ['last 20 versions']})]))
        .pipe(gulp.dest(cssDest))
);

gulp.task('style-prod', ['css-minify']);


gulp.task('production', ['scripts-prod', 'style-prod']);
