/**
 * Created by lucky on 06.11.16.
 */

const crypto = require('crypto');


/**
 *
 * @param {string} s
 * @returns {string}
 */
function hash(s) {
    if (!s) {
        throw new Error(`Not valid arg ${s}`);
    }
    return crypto.createHash('sha256')
        .update(s.toString())
        .digest('base64');
}


exports.hash = hash;



