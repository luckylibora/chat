/**
 * Created by lucky on 06.11.16.
 */

const express = require('express');
const path = require('path');
const auth = require('./auth');


const loginUrl = auth.loginUrl;
const mainUrl = auth.mainUrl;
const passport = auth.passport;
const router = express.Router();


router.get('/', (req, res, next) => res.redirect(loginUrl));


router.get(loginUrl, (req, res, next) => {
    return req.isAuthenticated() ? res.redirect(mainUrl) : res.render('login');
});


router.post(loginUrl, (req, res, next) => {
        return passport.authenticate('local', (err, user, info) => {
            if (err) {
                return next(err)
            }
            if (!user) {
                return res.render('login', {msg: 'Not valid login or password'});
            }
            return req.logIn(user, err => {
                    if (err) {
                        return next(err)
                    }
                    return res.redirect(mainUrl)
                }
            );
        })(req, res, next);
    }
);


router.get('/logout', (req, res, next) => {
    req.logout();
    req.session.destroy();
    return res.redirect(loginUrl);
});


router.get('/test', (req, res, next) => res.sendStatus(200));


module.exports = router;