const express = require('express');
const auth = require('./auth');
const error = require('./error');
const login = require('./login');
const main = require('./main');
const register = require('./register');
const views = require('./views');

const router = express.Router();


router.use(auth.passport.initialize());

router.use(auth.passport.session());

router.use('/', login);

router.use('/', register);

router.use('/', auth.userMw);

router.use('/', main);

router.use('/views', views);

router.use('/', error);


module.exports = router;
