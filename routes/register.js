/**
 * Created by lucky on 06.11.16.
 */

const express = require('express');
const auth = require('./auth');
const userService = require('../services/usersService');

const router = express.Router();


router.get('/register', (req, res, next) => {
    return req.isAuthenticated() ? res.redirect(auth.mainUrl) : res.render('register');
});


router.post('/register', (req, res, next) => {
    const body = req.body;
    const name = body.username;
    const password = body.password;
    return userService.create(name, password)
        .then(() => res.redirect(auth.loginUrl))
        .catch(err => res.render('register', err.message));
});



module.exports = router;