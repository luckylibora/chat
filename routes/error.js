/**
 * Created by lucky on 05.11.16.
 */

const express = require('express');
const intel = require('intel');


const router = express.Router();


router.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});


router.use((err, req, res, next) => {
    intel.error(err);
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.render('error');
});


module.exports = router;