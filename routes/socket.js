/**
 * Created by lucky on 06.11.16.
 */

const intel = require('intel');
const chatService = require('../services/chatsService');
const messagesService = require('../services/messagesService');


/**
 *
 * @param {Server} io
 * @returns {Server}
 */
module.exports = io => {

    /**
     *
     * personal room for each user
     * @param socket
     * @returns {string}
     */
    function userSocketRoom(socket){
        return 'user_' + socket.user;
    }


    io.on('connection', socket => {
        socket.on('disconnect', () => intel.info(`User ${socket.user} disconnected`))
            .on('login', () => {
                /**
                 * sync express and socket.io sessions
                 */
                const passport = socket.handshake.session.passport;
                if (!passport) {
                    return;
                }
                socket.user = passport.user;
                intel.info(`User ${socket.user} online`);
                socket.join(userSocketRoom(socket.user));
                chatService.getByUser(socket.user)
                    .then(chats => {
                        socket.emit('chats', chats);
                        /**
                         * subscribe to all user chats
                         */
                        chats.forEach(c => socket.join(c._id));
                    });
            })
            .on('sendMessage', data => {
                chatService.sendMessage(data.chatId, socket.user, data.message);
                io.to(data.chatId)
                    .emit('message', {user: socket.user, message: data.message, chatId: data.chatId, date: new Date()});
            })
            .on('chatMessages', data => {
                messagesService.getByChatId(data.id)
                    .then(msgs => socket.emit('chatMessages', msgs));
            })
            .on('getDialog', data => {
                chatService.getOrCreateDialog(socket.user, data.user)
                    .then(chat => {
                        socket.emit('getDialog', chat);
                        socket.join(chat._id);
                        io.to(userSocketRoom(data.user))
                            .emit('newChat', chat);
                    });
            })
            .on('newChat', data => {
                /**
                 * subscribe to new chat
                 */
                socket.join(data._id);
            });
    });
    return io;
};