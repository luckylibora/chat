/**
 * Created by lucky on 06.11.16.
 */

const passport = require('passport');
const passportLocal = require('passport-local');
const userService = require('../services/usersService');


const loginUrl = '/login';
const mainUrl = '/index';
const Strategy = passportLocal.Strategy;


passport.use(new Strategy(
    (userid, password, done) => userService.check(userid, password)
        .then(user => done(null, user))
        .catch(err => done(err, false))
));


passport.serializeUser((user, done) => done(null, user.name));


passport.deserializeUser((name, done) => {
    return userService.getByName(name)
        .then(user => done(null, user))
        .catch(err => done(err));
});


function userMw(req, res, next) {
    return req.isAuthenticated() ? next() : res.redirect(loginUrl);
}


exports.loginUrl = loginUrl;

exports.mainUrl = mainUrl;

exports.passport = passport;

exports.userMw = userMw;


