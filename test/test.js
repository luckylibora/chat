/**
 * Created by lucky on 07.11.16.
 */

const chai = require('chai');
const resources = require('../resources');

const assert = chai.assert;


const config = require('../config');


describe('Config', () => {

    describe('get mongo url',
        () => it('should return mongodb://localhost:27017/test_chat',
            () => assert.equal(config.mongo, 'mongodb://localhost:27017/test_chat')
        )
    );

    describe('get redis prefix',
        () => it('should return test:chat:',
            () => assert.equal(config.redis.prefix, 'test:chat:')
        )
    );

    describe('get session secret',
        () => it('should return test_secret',
            () => assert.equal(config.session.secret, 'test_secret')
        )
    );

});


const usersService = require('../services/usersService');
const usersCollection = require('../mongo/usersCollection');


describe('usersService', () => {

    before(done => {
        resources.init()
            .then(() => usersCollection.collection().removeMany({}))
            .then(() => done());
    });


    describe('#create', () => {

        it('should create user', done => {
            usersService.create('test', 'test')
                .then(() => usersService.getByName('test'))
                .then(user => {
                    assert.equal(user.name, 'test');
                    done()
                });
        });

        it('should hash user password', done => {
            usersService.create('testHash', 'test')
                .then(() => usersService.getByName('test'))
                .then(user => {
                    assert.notEqual(user.password, 'test');
                    done()
                });
        });

        it('should throw error with duplicate user names', done => {
            usersService.create('test', 'test')
                .then(() => {
                    assert.fail();
                    done()
                })
                .catch(err => {
                    assert.notEqual(err, null);
                    done()
                });
        });

        it('should throw error with null username', done => {
            usersService.create(null, 'test')
                .then(() => {
                    assert.fail();
                    done()
                })
                .catch(err => {
                    assert.notEqual(err, null);
                    done()
                });
        });

        it('should throw error with null password', done => {
            usersService.create('test', null)
                .then(() => {
                    assert.fail();
                    done()
                })
                .catch(err => {
                    assert.notEqual(err, null);
                    done()
                });
        });

        it('should throw error with empty username', done => {
            usersService.create('', 'test')
                .then(() => {
                    assert.fail();
                    done()
                })
                .catch(err => {
                    assert.notEqual(err, null);
                    done()
                });
        });

        it('should throw error with empty password', done => {
            usersService.create('test', '')
                .then(() => {
                    assert.fail();
                    done()
                })
                .catch(err => {
                    assert.notEqual(err, null);
                    done()
                });
        });

    });

    describe('#getByName', () => {

        it('should get user', done => {
            usersService.create('testGetByName', 'test')
                .then(() => usersService.getByName('testGetByName'))
                .then(user => {
                    assert.equal(user.name, 'testGetByName');
                    done()
                });
        });

        it('should throw error with null username', done => {
            usersService.getByName(null)
                .then(() => {
                    assert.fail();
                    done()
                })
                .catch(err => {
                    assert.notEqual(err, null);
                    done()
                });
        });

        it('should throw error with empty username', done => {
            usersService.getByName('')
                .then(() => {
                    assert.fail();
                    done()
                })
                .catch(err => {
                    assert.notEqual(err, null);
                    done()
                });
        });

    });

    describe('#check', () => {

        it('should check user', done => {
            usersService.create('testCheck', 'test')
                .then(() => usersService.check('testCheck', 'test'))
                .then(user => {
                    assert.equal(user.name, 'testCheck');
                    done()
                });
        });

        it('should throw error with null username', done => {
            usersService.check(null, 'test')
                .then(() => {
                    assert.fail();
                    done()
                })
                .catch(err => {
                    assert.notEqual(err, null);
                    done()
                });
        });

        it('should throw error with null password', done => {
            usersService.check('test', null)
                .then(() => {
                    assert.fail();
                    done()
                })
                .catch(err => {
                    assert.notEqual(err, null);
                    done()
                });
        });

        it('should throw error with empty username', done => {
            usersService.check('', 'test')
                .then(() => {
                    assert.fail();
                    done()
                })
                .catch(err => {
                    assert.notEqual(err, null);
                    done()
                });
        });

        it('should throw error with empty password', done => {
            usersService.check('test', '')
                .then(() => {
                    assert.fail();
                    done()
                })
                .catch(err => {
                    assert.notEqual(err, null);
                    done()
                });
        });

    });
});


