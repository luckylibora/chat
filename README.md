# README #

### Simple text web chat ###


## Running ##



```
#!bash

npm i
gulp          #gulp production for production


```

All config values should be in environment


* mongo - url to MongoDB server (ex. mongodb://localhost:27017/chat)
* redis_* - redis parameters, localhost:6379 by default. To specify redis port add redis_port=6380 to environment. Full list of redis connection parameters [here](https://github.com/NodeRedis/node_redis)
* session_* -  session parameters can be specified like redis parameters. Example: session_secret=my_secret. Full list [here](https://github.com/expressjs/session).
* PORT - webserver port. 3000 by default


### Example ###



```
#!bash

mongo=mongodb://localhost:27017/test_chat redis_prefix=test:chat: session_secret=test_secret node bin/www

```

I recommend to use pm2, supervisor or other process manager. They provide handy config to set up environment.



## Test ##


```
#!bash

npm test

```